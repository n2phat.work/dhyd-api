﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BVDHYD_API.Models
{
    [Table("utb_KetQuaCLS")]
    public class KQCLS
    {
        [Key, Column(Order = 0)]
        public string SoBienNhan { get; set; }
        [Key, Column(Order = 1)]
        public string SoChungTu { get; set; }
        [Key, Column(Order = 2)]
        public string SoHS { get; set; }
        public DateTime? NgayThucHien { get; set; }
        public string DVCode { get; set; }
        public int? LoaiBenhNhan { get; set; }
        public int? ID_CLS { get; set; }
        public string FileXML { get; set; }
    }
}