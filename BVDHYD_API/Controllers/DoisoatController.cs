﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BVDHYD_API.Models;

namespace BVDHYD_API.Controllers
{
    using Patterns;
    public class DoisoatController : ApiController
    {
        private BVContext db = new BVContext();
        [AcceptVerbs("POST")]
        [ResponseType(typeof(Response))]
        public Response insert([FromBody] ParamsDS doisoat)
        {
            Response re = new Response {
                status = true,
                data = null
            };
            
            return re;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}