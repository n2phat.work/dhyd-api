﻿using System;

namespace BVDHYD_API.Patterns
{
    public class ParamsTT
    {
        public Int64 IDThanhToan { get; set; }
        public string TransactionID_TT { get; set; }
        public int? SoTienThucThu { get; set; }
        public int? PhiDichVu { get; set; }
        public int? TongTien { get; set; }
        public string MaThe { get; set; }
        public Byte? LoaiThe { get; set; }
        public DateTime? NgayGioTT { get; set; }
        public Int16? TrangThai { get; set; }
    }
}