﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace BVDHYD_API.Models
{
    public class BacsiDTO
    {
        public Int64 Id { get; set; }
        public string HoTen { get; set; }
        public string ChucDanh { get; set; }
        public bool GioiTinh { get; set; }
        public Int16 IDBS { get; set; }
        public Nullable<DateTime> DateUpdate { get; set; }
        public string MaKhoa { get; set; }
        public virtual KhoaDTO Khoa {get; set;}
    }
}