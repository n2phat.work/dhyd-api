﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BVDHYD_API.Models
{
    [Table("utb_ToaThuoc")]
    public class Toathuoc
    {
        [Key]
        public string MaToaThuoc { get; set; }
        public string SoHS { get; set; }
        public DateTime NgayThucHien { get; set; }
        public int? LoaiBenhNhan { get; set; }
        public string FileXML { get; set; }
    }
}