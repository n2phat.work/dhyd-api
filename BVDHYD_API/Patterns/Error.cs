﻿using System;

namespace BVDHYD_API.Patterns
{
    public class Error
    {
        
        public int? error_code { get; set; }
        public string error_message { get; set; }
        public Error(int error_code, string error_message)
        {
            this.error_code = error_code;
            this.error_message = error_message;
        }
    }
}