﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BVDHYD_API.Models;

namespace BVDHYD_API.Controllers
{
    using Patterns;
    using System.Web.Script.Serialization;
    public class ToathuocController : ApiController
    {
        private BVContext db = new BVContext();

        [AcceptVerbs("GET", "POST")]
        [ResponseType(typeof(Response))]
        public Response getall(int? limit = 10, int? offset = 0)
        {
            Response re = new Response
            {
                status = true,
                data = null
            };

            var result = db.Toathuocs.ToList();
            re.data = result;
            return re;
        }

        [AcceptVerbs("GET", "POST")]
        [ResponseType(typeof(Response))]
        public Response search_by_sohs(string SoHS = null)
        {
            Response re = new Response
            {
                status = true,
                data = null
            };
            if (SoHS == null || SoHS == "")
            {
                re.status = false;
                return re;
            }
            var result = db.Toathuocs.Where(q => q.SoHS == SoHS);
            re.data = result;
            return re;
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}