﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace BVDHYD_API.Models
{
    [Table("utb_DoiSoatKham")]
    public class Doisoatkham
    {
        public Int64 Id { get; set; }
        public DateTime? NgayKham { get; set; }
        public int? IDBS { get; set; }
        public int? IDBuoiKham { get; set; }
        public string MaKhoa { get; set; }
        public string MedProID { get; set; }
        public string SoHS { get; set; }
        public string TransactionID_GD { get; set; }
        public int? SoTien { get; set; }
        public Int64 IdNhanBenhCT { get; set; }
        public int TrangThai { get; set; }
        public bool? DaXuLy { get; set; }
    }
}