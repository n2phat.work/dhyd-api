﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace BVDHYD_API.Models
{
    public class Payment
    {
        public Int64 Id { get; set; }
        public string transaction_id { get; set; }
        public int amount { get; set; }
        public string card_code { get; set; }
        public string card_name { get; set; }
    }
}