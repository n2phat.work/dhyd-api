﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace BVDHYD_API.Models
{
    public class BenhnhanDTO
    {
        public Benhnhan Benhnhan { get; set; }
        public Nghenghiep Nghenghiep { get; set; }
        public Tinh Tinh { get; set; }
        public Quanhuyen Quanhuyen { get; set; }
        public Phuongxa Phuongxa { get; set; }
        public Dantoc Dantoc { get; set; }
        public Quocgia Quocgia { get; set; }
    }
}