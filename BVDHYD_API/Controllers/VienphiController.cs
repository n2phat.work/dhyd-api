﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BVDHYD_API.Models;

namespace BVDHYD_API.Controllers
{
    using Patterns;
    using System.Web.Script.Serialization;
    public class VienphiController : ApiController
    {
        private BVContext db = new BVContext();

        [AcceptVerbs("GET", "POST")]
        [ResponseType(typeof(Response))]
        public Response getall(int? limit = 10, int? offset = 0)
        {
            Response re = new Response
            {
                status = true,
                data = null
            };

            var result = db.TamungVPs.ToList();
            re.data = result;
            return re;
        }

        [AcceptVerbs("GET", "POST")]
        [ResponseType(typeof(Response))]
        public Response searchmulti(string SoHS = null, string SoNV = null)
        {
            Response re = new Response
            {
                status = true,
                data = null
            };
            if (SoHS == null || SoHS == "")
            {
                re.status = false;
                return re;
            }
            var result = db.TamungVPs.Where(q => q.SoHS == SoHS);
            if (SoNV != null && SoNV != "")
            {
                result = result.Where(q => q.SoNV == SoNV);
            }
            re.data = result;
            return re;
        }

        [AcceptVerbs("GET", "POST")]
        [ResponseType(typeof(Response))]
        public Response searchsingle(string SoPhieu = null)
        {
            Response re = new Response
            {
                status = true,
                data = null
            };
            IQueryable<TamungVP> result = null;
            if (SoPhieu != null && SoPhieu != "")
            {
                result = db.TamungVPs.Where(q => q.SoPhieu == SoPhieu);
            }
            re.data = result;
            return re;
        }

        [AcceptVerbs("POST")]
        [ResponseType(typeof(Response))]
        public Response insert([FromBody] ParamsTTVP ttvp)
        {
            if (ttvp != null)
            {
                ThanhtoanVP vp = new ThanhtoanVP
                {
                    IDVienPhi = ttvp.IDVienPhi,
                    SoHS = ttvp.SoHS,
                    SoNV = ttvp.SoNV,
                    Ngay = ttvp.Ngay,
                    TraiBenh = ttvp.TraiBenh,
                    SoTien = ttvp.SoTien,
                    GhiChu = ttvp.GhiChu,
                    TrangThai = ttvp.TrangThai,
                    TransactionID_GD = ttvp.TransactionID_GD,
                    TransactionID_TT = ttvp.TransactionID_TT
                };
                db.ThanhtoanVPs.Add(vp);
                db.SaveChanges();
            }
            Response re = new Response
            {
                status = true,
                data = null
            };
            return re;
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(Response))]
        public Response history_by_sohs(String sohs = null)
        {
            IQueryable<TTVienPhi_ALL> his = null;
            his = db.TTVienPhi_ALLs.Where(p => p.SoHS == sohs);
            Response re = new Response
            {
                status = true,
                data = his
            };
            return re;
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(Response))]
        public Response updatetamung(Int64 id = 0, Int16 status = 0)
        {
            TamungVP tamung = db.TamungVPs.Where(p => p.Autoid == id).FirstOrDefault();
            if (tamung != null)
            {
                tamung.BTrangThai = status;
            }
            db.SaveChanges();
            Response re = new Response
            {
                status = true,
                data = tamung
            };
            return re;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}