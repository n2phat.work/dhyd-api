﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BVDHYD_API.Models
{
    [Table("mtb_TTVienPhi_ALL")]
    public class TTVienPhi_ALL
    {
        [Key]
        public Int64 IDTT { get; set; }
        public String SoPhieu { get; set; }
        public String SoHS { get; set; }
        public String SoNV { get; set; }
        public DateTime? Ngay { get; set; }
        public Int64? SoTien { get; set; }
        public Int64? TienMat { get; set; }
        public Int64? TienCK { get; set; }
        public Int64? TienBaoLanh { get; set; }
        public Int64? TienTaiTro { get; set; }
        public Int64? TienThe { get; set; }
        public Int64? TienTTThe_VTBank { get; set; }
        public String MaNhanVien { get; set; }
        public String NoiDung { get; set; }
    }
}