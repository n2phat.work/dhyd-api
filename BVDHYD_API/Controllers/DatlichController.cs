﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BVDHYD_API.Models;

namespace BVDHYD_API.Controllers
{
    using Patterns;
    public class DatlichController : ApiController
    {
        private BVContext db = new BVContext();

        [AcceptVerbs("POST")]
        [ResponseType(typeof(Response))]
        public Response updateDL([FromBody] TTTK datlich)
        {
            if (datlich.TransactionID_GD != null && datlich.TransactionID_GD != "")
            {
                TTTK dl = db.Datlichs.Where(e => e.TransactionID_GD == datlich.TransactionID_GD).FirstOrDefault();

                dl.NgayKham = datlich.NgayKham ?? dl.NgayKham;
                dl.RIDBuoiKham = datlich.RIDBuoiKham ?? dl.RIDBuoiKham;
                dl.MaDonVi = datlich.MaDonVi ?? dl.MaDonVi;
                dl.IDBS = datlich.IDBS ?? dl.IDBS;
                dl.IDDonViCT = datlich.IDDonViCT ?? dl.IDDonViCT;
                dl.DiaChi = datlich.DiaChi ?? dl.DiaChi;
                dl.GioKham = datlich.GioKham ?? dl.GioKham;
                dl.SoTT = datlich.SoTT ?? dl.SoTT;
                dl.SoHS = datlich.SoHS ?? dl.SoHS;
                dl.Ho = datlich.Ho ?? dl.Ho;
                dl.NamSinh = datlich.NamSinh ?? dl.NamSinh;
                dl.GioiTinh = datlich.GioiTinh ?? dl.GioiTinh;
                dl.BHYT = datlich.BHYT ?? dl.BHYT;
                dl.GhiChu = datlich.GhiChu ?? dl.GhiChu;
                dl.Ten = datlich.Ten ?? dl.Ten;
                dl.NgaySinh = datlich.NgaySinh ?? dl.NgaySinh;
                dl.SoTien = datlich.SoTien ?? dl.SoTien;
                dl.TrangThai = datlich.TrangThai ?? dl.TrangThai;
                dl.MedProID = datlich.MedProID ?? dl.MedProID;
                dl.TransactionID_TT = datlich.TransactionID_TT ?? dl.TransactionID_TT;
                dl.DienThoai = datlich.DienThoai ?? dl.DienThoai;
                dl.Email = datlich.Email ?? dl.Email;
                dl.IDTinh = datlich.IDTinh ?? dl.IDTinh;
                dl.IDQuanHuyen = datlich.IDQuanHuyen ?? dl.IDQuanHuyen;
                dl.IDPhuongXa = datlich.IDPhuongXa ?? dl.IDPhuongXa;
                dl.IDDanToc = datlich.IDDanToc ?? dl.IDDanToc;
                dl.IDNgheNghiep = datlich.IDNgheNghiep ?? dl.IDNgheNghiep;
                dl.MaQuocGia = datlich.MaQuocGia ?? dl.MaQuocGia;
            }
            db.SaveChanges();
            Response re = new Response
            {
                status = true
            };
            return re;
        }

        [AcceptVerbs("POST")]
        [ResponseType(typeof(Response))]
        public Response insertDL([FromBody] TTTK datlich)
        {
            if (datlich != null)
            {
                TTTK dl = new TTTK
                {
                    NgayKham = datlich.NgayKham,
                    RIDBuoiKham = datlich.RIDBuoiKham,
                    MaDonVi = datlich.MaDonVi,
                    IDBS = datlich.IDBS,
                    SoTT = datlich.SoTT,
                    SoHS = datlich.SoHS,
                    Ho = datlich.Ho,
                    NamSinh = datlich.NamSinh,
                    GioiTinh = datlich.GioiTinh,
                    BHYT = datlich.BHYT,
                    GhiChu = datlich.GhiChu,
                    Ten = datlich.Ten,
                    NgaySinh = datlich.NgaySinh,
                    SoTien = datlich.SoTien,
                    TrangThai = datlich.TrangThai,
                    MedProID = datlich.MedProID,
                    TransactionID_GD = datlich.TransactionID_GD,
                    TransactionID_TT = datlich.TransactionID_TT,
                    DienThoai = datlich.DienThoai,
                    Email = datlich.Email,
                    IDTinh = datlich.IDTinh,
                    IDQuanHuyen = datlich.IDQuanHuyen,
                    IDPhuongXa = datlich.IDPhuongXa,
                    IDDanToc = datlich.IDDanToc,
                    IDNgheNghiep = datlich.IDNgheNghiep,
                    MaQuocGia = datlich.MaQuocGia,
                    IDDonViCT = datlich.IDDonViCT,
                    DiaChi = datlich.DiaChi,
                    GioKham = datlich.GioKham
                };
                BVContext tmp_db = new BVContext();
                Thanhtoan tt = tmp_db.Thanhtoans.Where(e => e.TransactionID_TT == dl.TransactionID_TT && e.TrangThai == dl.TrangThai).FirstOrDefault();
                if (tt != null)
                    dl.IDThanhToan = tt.IDThanhToan;
                db.Datlichs.Add(dl);
                db.SaveChanges();
            }
            Response re = new Response
            {
                status = true
            };
            return re;
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(Response))]
        public Response map_thanhtoan()
        {
            var dls = db.Datlichs.Where(e => e.IDThanhToan == 0);
            foreach (var item in dls)
            {
                BVContext tmp_db = new BVContext();
                Thanhtoan tt = tmp_db.Thanhtoans.Where(e => e.TransactionID_TT == item.TransactionID_TT).SingleOrDefault();
                if (tt != null)
                    item.IDThanhToan = tt.IDThanhToan;
            }
            db.SaveChanges();
            Response re = new Response
            {
                status = true
            };
            return re;
        }

        [AcceptVerbs("POST", "GET")]
        [ResponseType(typeof(Response))]
        public Response updateDL(String transaction_code_gd, int trangthai = 0)
        {
            Response re = new Response();
            if (transaction_code_gd != null)
            {
                var datlich = (from p in db.Datlichs where p.TransactionID_GD == transaction_code_gd select p).SingleOrDefault();
                if (datlich != null)
                    datlich.TrangThai = trangthai;
                db.SaveChanges();
            }
            return re;
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(Response))]
        public Response getDLbyMedproID(string medpro_id)
        {
            Response re = new Response { status = true };
            re.data = (from p in db.Datlichs where p.MedProID == medpro_id select p).SingleOrDefault();
            return re;
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(Response))]
        public Response getDLbyTransactionGD(string transaction_code_gd)
        {
            Response re = new Response { status = true };
            re.data = (from p in db.Datlichs where p.TransactionID_GD == transaction_code_gd select p).SingleOrDefault();
            return re;
        }

        [AcceptVerbs("POST")]
        [ResponseType(typeof(Response))]
        public Response insertTT([FromBody] ParamsTT thanhtoan)
        {
            if (thanhtoan != null)
            {
                Thanhtoan tt = new Thanhtoan
                {
                    TransactionID_TT = thanhtoan.TransactionID_TT,
                    SoTienThucThu = thanhtoan.SoTienThucThu,
                    MaThe = thanhtoan.MaThe,
                    LoaiThe = thanhtoan.LoaiThe,
                    NgayGioTT = thanhtoan.NgayGioTT,
                    NgayTT = thanhtoan.NgayGioTT,
                    PhiDichVu = thanhtoan.PhiDichVu,
                    TrangThai = thanhtoan.TrangThai,
                    TongTien = thanhtoan.TongTien
                };
                db.Thanhtoans.Add(tt);
                db.SaveChanges();
            }
            Response re = new Response
            {
                status = true
            };
            return re;
        }

        [AcceptVerbs("POST", "GET")]
        [ResponseType(typeof(Response))]
        public Response updateTT(Int64? transaction_code_tt = null, Int16 trangthai = 0)
        {
            Response re = new Response();
            if (transaction_code_tt != null)
            {
                var thanhtoan = (from p in db.Thanhtoans where p.IDThanhToan == transaction_code_tt select p).SingleOrDefault();
                if (thanhtoan != null)
                    thanhtoan.TrangThai = trangthai;
                db.SaveChanges();
            }
            return re;
        }

        [AcceptVerbs("POST")]
        [ResponseType(typeof(Response))]
        public Response insertDSK([FromBody] ParamsDSK datsokham)
        {
            if (datsokham != null)
            {
                DatSoKham new_dsk = new DatSoKham
                {
                    MaDonVi = datsokham.MaDonVi,
                    RIDBuoiKham = datsokham.RIDBuoiKham,
                    IDBS = datsokham.IDBS,
                    NgayKham = datsokham.NgayKham,
                    SoTT = datsokham.SoTT,
                    IDDonViCT = datsokham.IDDonViCT,
                    GioKham = datsokham.GioKham,
                    TransactionID_GD = datsokham.TransactionID_GD,
                    TransactionID_TT = datsokham.TransactionID_TT,
                    Loai = datsokham.Loai,
                    CDate = datsokham.CDate
                };
                db.DatSoKhams.Add(new_dsk);
                db.SaveChanges();
            }
            Response re = new Response
            {
                status = true
            };
            return re;
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(Response))]
        public Response getbookedDSK(String MaDonVi, byte RIDBuoiKham, Int16 IDBS, DateTime NgayKham)
        {
            var dsk = from p in db.DatSoKhams where (p.MaDonVi == MaDonVi & p.RIDBuoiKham == RIDBuoiKham & p.IDBS == IDBS & p.NgayKham == NgayKham) select p;
            Response re = new Response
            {
                status = true,
                data = dsk
            };
            return re;
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(Response))]
        public Response delete_unusedDSK(DateTime date_to_delete)
        {
            var old2 = db.DatSoKhams.Where(e => e.NgayKham == date_to_delete).Where(e => e.TransactionID_GD == null || e.TransactionID_GD == "");
            var result = db.DatSoKhams.RemoveRange(old2);
            db.SaveChanges();
            Response re = new Response
            {
                status = true,
            };
            return re;
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(Response))]
        public Response deleteDSK(String transaction_code_gd)
        {
            var data = db.DatSoKhams.Where(e => e.TransactionID_GD == transaction_code_gd);
            var result = db.DatSoKhams.RemoveRange(data);
            db.SaveChanges();
            Response re = new Response
            {
                status = true,
            };
            return re;
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(Response))]
        public Response nullifyDSK(String transaction_code_gd)
        {
            var data = db.DatSoKhams.Where(e => e.TransactionID_GD == transaction_code_gd).ToList();
            data.ForEach(e => {
                e.TransactionID_GD = null;
                e.TransactionID_TT = null;
            });
            db.SaveChanges();
            Response re = new Response
            {
                status = true,
            };
            return re;
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(Response))]
        public Response get_sohs_from_tttk(String medpro_id)
        {
            var data = db.Datlichs.Where(e => e.MedProID == medpro_id).ToList();
            Response re = new Response
            {
                status = true,
                data = data
            };
            return re;
        }
    }
}