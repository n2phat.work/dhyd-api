﻿using System;

namespace BVDHYD_API.Patterns
{
    public class BNQuery
    {
        public int Id { get; set; }
        public string SoHS { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public bool? GioiTinh { get; set; }
        public short? IDTinh { get; set; }
        public short? IDQuanHuyen { get; set; }
        public short? IDPhuongXa { get; set; }
        public short? NamSinh { get; set; }
        public DateTime? NgaySinh { get; set; }
    }
}