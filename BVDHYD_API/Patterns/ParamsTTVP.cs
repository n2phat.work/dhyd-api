﻿using System;

namespace BVDHYD_API.Patterns
{
    public class ParamsTTVP
    {
        public Int64 IDTT { get; set; }
        public Int64 IDVienPhi { get; set; }
        public string SoHS { get; set; }
        public string SoNV { get; set; }
        public DateTime Ngay { get; set; }
        public string TraiBenh { get; set; }
        public Int64 SoTien { get; set; }
        public string GhiChu { get; set; }
        public int? TrangThai { get; set; }
        public string TransactionID_GD { get; set; }
        public string TransactionID_TT { get; set; }
        public DateTime Cdate { get; set; }
    }
}