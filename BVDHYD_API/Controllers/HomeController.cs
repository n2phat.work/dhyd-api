﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using BVDHYD_API.Models;
using System.Linq;

namespace BVDHYD_API.Controllers
{
    using Patterns;
    using System.Web.Script.Serialization;
    public class HomeController : ApiController
    {
        [AcceptVerbs("GET", "POST")]
        [ResponseType(typeof(Response))]
        public Response getdata(String table_name = null, int? limit = 10, int? offset = 0)
        {
            StoredContainer storecontext = new StoredContainer();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            Object result = null;
            if (table_name != null && table_name != "")
            {
                result = json_serializer.DeserializeObject(storecontext.sp_select_table(table_name, limit, offset).FirstOrDefault());
            }
            Response re = new Response
            {
                status = true,
                data = result
            };

            return re;
        }

        [AcceptVerbs("GET", "POST")]
        [ResponseType(typeof(Response))]
        public Response gettables()
        {
            StoredContainer storecontext = new StoredContainer();
            Object result = storecontext.sp_get_tables();
            
            Response re = new Response
            {
                status = true,
                data = result
            };

            return re;
        }

        [ResponseType(typeof(Object))]
        [HttpGet]
        public IHttpActionResult index()
        {
            Info a = new Info {
                message = "API..."
            };
            
            return Ok(a);
        }
        [HttpGet]
        [HttpPost]
        public Error notfound()
        {
            return new Error(404, "notfound");
        }
    }
}
