﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BVDHYD_API.Models
{
    [Table("mtb_ThanhToan_TienKham")]
    public class TTTK
    {
        [Key]
        public Int64 IDTTTienKham { get; set; }
        public Int64 IDThanhToan { get; set; }
        public string SoHS { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public bool? GioiTinh { get; set; }
        public int? NamSinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public string DiaChi { get; set; }
        public DateTime? NgayKham { get; set; }
        public string MaDonVi { get; set; }
        public byte? RIDBuoiKham { get; set; }
        public int? IDBS { get; set; }
        public int? IDDonViCT { get; set; }
        public int? SoTT { get; set; }
        public TimeSpan? GioKham { get; set; }
        public int? SoTien { get; set; }
        public bool? BHYT { get; set; }
        public string GhiChu { get; set; }
        public int? TrangThai { get; set; }
        public string TransactionID_GD { get; set; }
        public string TransactionID_TT { get; set; }
        public string MedProID { get; set; }
        public string DienThoai { get; set; }
        public string Email { get; set; }
        public Int16? IDNgheNghiep { get; set; }
        public Int16? IDTinh { get; set; }
        public Int16? IDQuanHuyen { get; set; }
        public Int16? IDPhuongXa { get; set; }
        public Int16? IDDanToc { get; set; }
        public string MaQuocGia { get; set; }
    }
}