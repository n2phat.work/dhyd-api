﻿using System;

namespace BVDHYD_API.Patterns
{
    public class ParamsDS
    {
        public string SoHS { get; set; }
        public string MedProID { get; set; }
        public Int64 IDNhanBenhCT { get; set; }
        public string TransactionID_GD { get; set; }
    }
}