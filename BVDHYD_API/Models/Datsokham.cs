﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BVDHYD_API.Models
{
    [Table("mtb_DatSoKham")]
    public class DatSoKham
    {
        [Key, Column(Order = 0)]
        public string MaDonVi { get; set; }
        [Key, Column(Order = 1)]
        public byte RIDBuoiKham { get; set; }
        [Key, Column(Order = 2)]
        public Int16 IDBS { get; set; }
        [Key, Column(Order = 3)]
        public DateTime NgayKham { get; set; }
        [Key, Column(Order = 4)]
        public Int16 SoTT { get; set; }
        public int? IDDonViCT { get; set; }
        public TimeSpan? GioKham { get; set; }
        public string TransactionID_GD { get; set; }
        public string TransactionID_TT { get; set; }
        public byte? Loai { get; set; }
        public DateTime CDate { get; set; }
        public string CHost { get; set; }
    }
}