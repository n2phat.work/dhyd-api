﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace BVDHYD_API.Models
{
    [Table("DM_BacSi")]
    public class Bacsi
    {
        public Int64 Id { get; set; }
        public string HoTen { get; set; }
        public string Ten { get; set; }
        public string ChucDanh { get; set; }
        public string TenHinh { get; set; }
        public bool GioiTinh { get; set; }
        public Int16 IDBS { get; set; }
    }
}