﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BVDHYD_API.Models
{
    [Table("mtb_BenhNhan_Update")]
    public class BenhnhanUpdate
    {
        [Key]
        public int Id { get; set; }
        public String SoHS { get; set; }
        public String Ho { get; set; }
        public String Ten { get; set; }
        public Boolean? GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public Int16? NamSinh { get; set; }
        public String DienThoai { get; set; }
        public String Email { get; set; }
        public Int16? IDNgheNghiep { get; set; }
        public String MaQuocGia { get; set; }
        public Int16? IDTinh { get; set; }
        public Int16? IDQuanHuyen { get; set; }
        public Int16? IDPhuongXa { get; set; }
        public String DiaChi { get; set; }
        public Int16 IDDanToc { get; set; }
        public String DiaChiHIS { get; set; }
        public String HoVni { get; set; }
        public String TenVni { get; set; }
        public String DiaChiVni { get; set; }
        public int? CUser { get; set; }
        public String SoNhapVien { get; set; }
        public int? IDDonViKSK { get; set; }
        public String Nghe { get; set; }
        public Boolean? NgungSD { get; set; }
        public String DiDong { get; set; }
        public String SoCMND { get; set; }
        public String DiaChiHISEN { get; set; }
        public String MedProID { get; set; }
    }
}