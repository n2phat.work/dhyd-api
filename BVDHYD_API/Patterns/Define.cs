﻿using System;

namespace BVDHYD_API.Patterns
{
    public class PaymentType
    {
        public int THE_KB = 1;
        public int THE_TD = 2;
        public int THE_ATM = 3;

    }

    public class Define
    {
        public static PaymentType payment_type = new PaymentType();
    }
}