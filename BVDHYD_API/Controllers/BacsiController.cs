﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BVDHYD_API.Models;

namespace BVDHYD_API.Controllers
{
    using Patterns;
    using System.Web.Script.Serialization;
    public class BacsiController : ApiController
    {
        private BVContext db = new BVContext();
        
        [AcceptVerbs("GET", "POST")]
        [ResponseType(typeof(Response))]
        public Response getall(int? limit = 10, int? offset = 0)
        {
            Response re = new Response
            {
                status = true,
                data = null
            };

            StoredContainer storecontext = new StoredContainer();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            Object result = json_serializer.DeserializeObject(storecontext.sp_select_doctor(limit, offset).FirstOrDefault());
            re.data = result;
            return re;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}