﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace BVDHYD_API.Models
{
    [Table("utb_DatLich")]
    public class Datlich
    {
        public Int64 Id { get; set; }
        public DateTime? NgayKham { get; set; }
        public int? IDBuoiKham { get; set; }
        public string MaKhoa { get; set; }
        public int? IDBS { get; set; }
        public int? SoKham { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public bool? Gioi { get; set; }
        public bool? BHYT { get; set; }
        public DateTime? NgaySinh { get; set; }
        public int? NamSinh { get; set; }
        public string GhiChu { get; set; }
        public int TrangThai { get; set; }
        public string MedProID { get; set; }
        public int? SoTien { get; set; }
        public string TransactionID_GD { get; set; }
        public string TransactionID_TT { get; set; }
    }
}