﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BVDHYD_API.Models
{
    [Table("utb_Bafee_TongHop_HoaDon")]
    public class Hoadon
    {
        [Key]
        public Int64 ID { get; set; }
        public string MaPhieu { get; set; }
        public int? Ngay { get; set; }
        public string SoHoSo { get; set; }
        public string SoNhapVien { get; set; }
        public string MaHoaDon { get; set; }
        public Decimal SoTien { get; set; }
        public byte? IDTinhTrang { get; set; }
        public string MaEinvoice { get; set; }
    }
}