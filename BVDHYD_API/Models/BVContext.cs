﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration.Conventions;
using BVDHYD_API.Models;

namespace BVDHYD_API.Models
{
    public class BVContext : DbContext
    {
        public BVContext() : base("BVContext")
        {
            this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
            Database.SetInitializer<BVContext>(null);
        }
        public DbSet<TTTK> Datlichs { get; set; }
        public DbSet<Thanhtoan> Thanhtoans { get; set; }
        public DbSet<DatSoKham> DatSoKhams { get; set; }
        public DbSet<TamungVP> TamungVPs { get; set; }
        public DbSet<ThanhtoanVP> ThanhtoanVPs { get; set; }
        public DbSet<TTVienPhi_ALL> TTVienPhi_ALLs { get; set; }
        public DbSet<KQCLS> KQCLSs { get; set; }
        public DbSet<Toathuoc> Toathuocs { get; set; }
        public DbSet<Hoadon> Hoadons { get; set; }

        public DbSet<BenhnhanUpdate> BenhnhanUpdates { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
