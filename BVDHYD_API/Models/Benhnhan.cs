﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace BVDHYD_API.Models
{
    [Table("utb_Benhnhan")]
    public class Benhnhan
    {
        public int Id { get; set; }
        public string SoHS { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public bool? GioiTinh { get; set; }
        public Nullable<DateTime> NgaySinh { get; set; }
        public short? NamSinh { get; set; }
        public string DienThoai { get; set; }
        public string Email { get; set; }
        public short? IDNgheNghiep { get; set; }
        public string MaQuocGia { get; set; }
        public short? IDTinh { get; set; }
        public short? IDQuanHuyen { get; set; }
        public short? IDPhuongXa { get; set; }
        public string DiaChi { get; set; }
        public short? IDDanToc { get; set; }
        public string DiaChiHIS { get; set; }
        public string HoVni { get; set; }
        public string TenVni { get; set; }
        public string DiaChiVni { get; set; }
        public int? CUser { get; set; }
        public string SoNhapVien { get; set; }
        public int? IDDonViKSK { get; set; }
        public string Nghe { get; set; }
        public bool? NgungSD { get; set; }
        public string DiDong { get; set; }
        public string SoCMND { get; set; }
        public string DiaChiHISEN { get; set; }
        public string MedProID { get; set; }
        //public virtual Nghenghiep Nghenghiep { get; set; }
        //public virtual Dantoc Dantoc { get; set; }
        //public virtual Phuongxa Phuongxa { get; set; }
        //public virtual Quanhuyen Quanhuyen { get; set; }
        //public virtual Tinh Tinh { get; set; }
        //public virtual Quocgia Quocgia { get; set; }
    }
}