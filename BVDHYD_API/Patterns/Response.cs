﻿using System;

namespace BVDHYD_API.Patterns
{
    public class Response
    {
        public Object data { get; set;}
        public bool status { get; set; }
    }
}