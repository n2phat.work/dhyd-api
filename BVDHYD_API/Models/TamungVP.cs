﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BVDHYD_API.Models
{
    [Table("mtb_TamUngVienPhi")]
    public class TamungVP
    {
        [Key]
        public Int64 Autoid { get; set; }
        public string SoPhieu { get; set; }
        public string SoHS { get; set; }
        public string SoNV { get; set; }
        public DateTime? Ngay { get; set; }
        public Int64? SoTien { get; set; }
        public string Sobn { get; set; }
        public string TraiBenh { get; set; }
        public byte TTXV { get; set; }
        public string SoDT { get; set; }
        public string MaNhanVien { get; set; }
        public Int16? BTrangThai { get; set; }
        public DateTime? Cdate { get; set; }
        public bool? bdel { get; set; }
        public string TenPhong { get; set; }
        public string ChanDoan { get; set; }
        public string TenGiuong { get; set; }
        public string NoiDung { get; set; }
    }
}