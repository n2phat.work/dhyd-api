﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BVDHYD_API.Models;
using System.Web.Script.Serialization;

namespace BVDHYD_API.Controllers
{
    using Patterns;
    public class BenhnhanController : ApiController
    {
        private BVContext db = new BVContext();

        // GET: api/Benhnhans
        public Response getall()
        {
            Response re = new Response {
                status = true,
                data = null
            };
            
            return re;
        }

        [AcceptVerbs("POST")]
        [ResponseType(typeof(Response))]
        public Response advanced_search([FromBody] BNQuery benhnhan)
        {
            StoredContainer storecontext = new StoredContainer();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var result = json_serializer.DeserializeObject(storecontext.usp_Get_BenhNhan_GetThongTin(benhnhan.Ho, benhnhan.Ten, benhnhan.GioiTinh, benhnhan.NamSinh, benhnhan.IDTinh).FirstOrDefault());
            Response re = new Response
            {
                data = result,
                status = true
            };
            return re;
        }

        [AcceptVerbs("POST")]
        [ResponseType(typeof(Response))]
        public Response advanced_search2([FromBody] BNQuery benhnhan)
        {
            StoredContainer storecontext = new StoredContainer();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var query = storecontext.usp_Get_BenhNhan_GetThongTin_PhuongXa(benhnhan.Ho, benhnhan.Ten, benhnhan.GioiTinh, benhnhan.NamSinh, benhnhan.IDTinh, benhnhan.IDQuanHuyen, benhnhan.IDPhuongXa).FirstOrDefault();
            var result = (query != null) ? json_serializer.DeserializeObject(query) : null;
            Response re = new Response
            {
                data = result,
                status = true
            };
            return re;
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(Response))]
        public Response getbyhsbn(string sohs = null)
        {
            StoredContainer storecontext = new StoredContainer();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var result = json_serializer.DeserializeObject(storecontext.usp_Get_BenhNhan_SoHoSo(sohs).FirstOrDefault());

            Response re = new Response {
                data = result,
                status = true
            };
            return re;
        }

        [AcceptVerbs("POST")]
        [ResponseType(typeof(Response))]
        public Response insert_bn_update([FromBody] BenhnhanUpdate bn_insert)
        {
            BenhnhanUpdate bn_update = new BenhnhanUpdate();
            bn_update.SoHS = bn_insert.SoHS;
            bn_update.Ho = bn_insert.Ho;
            bn_update.Ten = bn_insert.Ten;
            bn_update.GioiTinh = bn_insert.GioiTinh;
            bn_update.NgaySinh = bn_insert.NgaySinh;
            bn_update.NamSinh = bn_insert.NamSinh;
            bn_update.DienThoai = bn_insert.DienThoai;
            bn_update.Email = bn_insert.Email;
            bn_update.IDNgheNghiep = bn_insert.IDNgheNghiep;
            bn_update.MaQuocGia = bn_insert.MaQuocGia;
            bn_update.IDTinh = bn_insert.IDTinh;
            bn_update.IDQuanHuyen = bn_insert.IDQuanHuyen;
            bn_update.IDPhuongXa = bn_insert.IDPhuongXa;
            bn_update.DiaChi = bn_insert.DiaChi;
            bn_update.IDDanToc = bn_insert.IDDanToc;
            bn_update.DiaChiHIS = bn_insert.DiaChiHIS;
            bn_update.HoVni = bn_insert.HoVni;
            bn_update.TenVni = bn_insert.TenVni;
            bn_update.DiaChiVni = bn_insert.DiaChiVni;
            bn_update.CUser = bn_insert.CUser;
            bn_update.SoNhapVien = bn_insert.SoNhapVien;
            bn_update.IDDonViKSK = bn_insert.IDDonViKSK;
            bn_update.Nghe = bn_insert.Nghe;
            bn_update.NgungSD = bn_insert.NgungSD;
            bn_update.DiDong = bn_insert.DiDong;
            bn_update.SoCMND = bn_insert.SoCMND;
            bn_update.DiaChiHISEN = bn_insert.DiaChiHISEN;
            bn_update.MedProID = bn_insert.MedProID;
            db.BenhnhanUpdates.Add(bn_update);
            db.SaveChanges();

            Response re = new Response
            {
                data = null,
                status = true
            };
            return re;
        }

        [AcceptVerbs("GET")]
        [ResponseType(typeof(Response))]
        public Response delete_bn_update(String so_hs)
        {
            var data = db.BenhnhanUpdates.Where(e => e.SoHS == so_hs);
            var result = db.BenhnhanUpdates.RemoveRange(data);
            db.SaveChanges();
            Response re = new Response
            {
                status = true,
            };
            return re;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}