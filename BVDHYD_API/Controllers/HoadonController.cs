﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BVDHYD_API.Models;

namespace BVDHYD_API.Controllers
{
    using Patterns;
    using System.Web.Script.Serialization;
    public class HoadonController : ApiController
    {
        private BVContext db = new BVContext();
        [AcceptVerbs("GET", "POST")]
        [ResponseType(typeof(Response))]

        public Response gethd(string eInvoiceID = null)
        {
            Response re = new Response
            {
                status = true,
                data = null
            };
            if (eInvoiceID == null || eInvoiceID == "")
            {
                re.status = false;
                return re;
            }
            //  SR_HoaDon. objXuatHoaDon = new EinvoiceService();
            // goi lay hoa don tai day
            string urlService = "http://hoadonvat_services:8888/EinvoiceService.asmx";
            string base64String = "";
            var result = urlService.Substring(0, urlService.Length - 21) + "/HoaDonpdf.aspx?mhd=" + eInvoiceID;
            using (WebClient client = new WebClient())
            {
                var bytes = client.DownloadData(result);
                base64String = Convert.ToBase64String(bytes);
            }
            // end
            re.data = base64String;
            return re;
        }

        [AcceptVerbs("GET", "POST")]
        [ResponseType(typeof(Response))]

        public Response getbysohs(string sohs = null)
        {
            Response re = new Response
            {
                status = true,
                data = null
            };
            if (sohs == null || sohs == "")
            {
                re.status = false;
                return re;
            }
            var result = db.Hoadons.Where(q => q.SoHoSo == sohs);
            re.data = result;
            return re;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}