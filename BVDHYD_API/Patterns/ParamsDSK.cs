﻿using System;

namespace BVDHYD_API.Patterns
{
    public class ParamsDSK
    {
        public string MaDonVi { get; set; }
        public byte RIDBuoiKham { get; set; }
        public Int16 IDBS { get; set; }
        public DateTime NgayKham { get; set; }
        public Int16 SoTT { get; set; }
        public int? IDDonViCT { get; set; }
        public TimeSpan? GioKham { get; set; }
        public string TransactionID_GD { get; set; }
        public string TransactionID_TT { get; set; }
        public byte? Loai { get; set; }
        public DateTime CDate { get; set; }
    }
}