﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BVDHYD_API.Models
{
    [Table("mtb_ThanhToan")]
    public class Thanhtoan
    {
        [Key]
        public Int64 IDThanhToan { get; set; }
        public string TransactionID_TT { get; set; }
        public int? SoTienThucThu { get; set; }
        public int? PhiDichVu { get; set; }
        public int? TongTien { get; set; }
        public string MaThe { get; set; }
        public Byte? LoaiThe { get; set; }
        public DateTime? NgayGioTT { get; set; }
        public DateTime? NgayTT { get; set; }
        public Int16? TrangThai { get; set; }
    }
}