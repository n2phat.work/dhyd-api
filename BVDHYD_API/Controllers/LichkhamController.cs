﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BVDHYD_API.Models;

namespace BVDHYD_API.Controllers
{
    using Patterns;
    using System.Web.Script.Serialization;
    public class LichkhamController : ApiController
    {
        private BVContext db = new BVContext();
        private StoredContainer stored = new StoredContainer();
        private JavaScriptSerializer json_serializer = new JavaScriptSerializer();

        [AcceptVerbs("GET", "POST")]
        [ResponseType(typeof(Response))]
        public Response getlichbs(int? limit = 10, int? offset = 0)
        {
            Object result = json_serializer.DeserializeObject(stored.sp_select_lichkham(limit, offset).FirstOrDefault());
            Response res = new Response {
                data = result,
                status = true
            };
            
            return res;
        }

        public Response getlichbsthaythe(int? limit = 10, int? offset = 0)
        {
            Object result = json_serializer.DeserializeObject(stored.sp_select_lichkhamthe(limit, offset).FirstOrDefault());
            Response res = new Response
            {
                data = result,
                status = true
            };

            return res;
        }

        public Response getdutrukham(int? limit = 10, int? offset = 0)
        {
            Object result = json_serializer.DeserializeObject(stored.sp_select_dutrukham(limit, offset).FirstOrDefault());
            Response res = new Response
            {
                data = result,
                status = true
            };

            return res;
        }
    }
}